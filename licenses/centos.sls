{% from slspath + "/map.jinja" import licenses with context %}

{%- set filename = licenses.directory + '/licences-$(date +%F).csv' %}

collect licenses:
  cmd.run:
    - names:
      - echo "\"package\";\"version\";\"license\"" > {{ filename }}
      - rpm -qa --qf "\"%{name}\";\"%{version}\";\"%{license}\"\n" | sort >> {{ filename }}