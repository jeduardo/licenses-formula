{% from slspath + "/map.jinja" import licenses with context %}

{%- set filename = licenses.directory + '/licences-$(date +%F).csv' %}

deploy dpkg-licenses:
  file.recurse:
    - name: {{ licenses.directory }}/dpkg-licenses
    - source: salt://{{ slspath }}/files/dpkg-licenses
    - file_mode: keep
    - user: root
    - group: root

collect licenses:
  cmd.run:
    - names:
      - echo "\"package\";\"version\";\"license\"" > {{ filename }}
      - {{ licenses.directory }}/dpkg-licenses/dpkg-licenses | grep ii | sed 's/^"ii";//g'| sort >> {{ filename }}
    - require:
      - file: deploy dpkg-licenses